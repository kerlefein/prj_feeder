import requests
from requests.exceptions import RequestException
import logging


logger = logging.getLogger(__name__)


class NASAClient:
    def __init__(self, api_key):
        self.api_key = api_key

    def retrieve_apod(self) -> object:
        url = 'https://api.nasa.gov/planetary/apod?api_key={}'.format(self.api_key)
        try:
            response = requests.get(url)
        except RequestException as ex:
            logger.exception(ex)
            return False
        pic = response.json()
        return APODDetails(
            pic.get('date', None),
            pic.get('explanation', None),
            pic.get('title', None),
            pic.get('copyright', None),
            pic.get('url', None),
            pic.get('hdurl', None),
        )

    def retrieve_insight(self):
        url = 'https://api.nasa.gov/insight_weather/?api_key={}&feedtype=json&ver=1.0'.format(self.api_key)
        try:
            response = requests.post(url)
        except RequestException as ex:
            logger.exception(ex)
            return False
        mars_weather = response.json()
        last_valid_sol = max(mars_weather['sol_keys'])
        return InSightDetaild(
            last_valid_sol,
            mars_weather[last_valid_sol]['Season'],
            mars_weather[last_valid_sol]['First_UTC'],
            mars_weather[last_valid_sol]['Last_UTC'],
            mars_weather[last_valid_sol]['AT']['av'],
            mars_weather[last_valid_sol]['AT']['mn'],
            mars_weather[last_valid_sol]['AT']['mx'],
            mars_weather[last_valid_sol]['HWS']['av'],
            mars_weather[last_valid_sol]['HWS']['mn'],
            mars_weather[last_valid_sol]['HWS']['mx'],
            mars_weather[last_valid_sol]['PRE']['av'],
            mars_weather[last_valid_sol]['PRE']['mn'],
            mars_weather[last_valid_sol]['PRE']['mx'],
            mars_weather[last_valid_sol]['WD']['most_common']['compass_point'],
        )


class APODDetails:
    def __init__(self, date, explanation, title, author, img, img_hd):
        self.date = date
        self.explanation = explanation
        self.title = title
        self.author = author
        self.img = img
        self.img_hd = img_hd


class InSightDetaild:
    def __init__(self, sol, season, first_utc, last_utc, AT_av, AT_mn, AT_mx, HWS_av, HWS_mn, HWS_mx, PRE_av, PRE_mn, PRE_mx, WD_cp):
        self.sol = sol
        self.season = season
        self.first_utc = first_utc
        self.last_utc = last_utc
        self.AT_av = AT_av
        self.AT_mn = AT_mn
        self.AT_mx = AT_mx
        self.HWS_av = HWS_av
        self.HWS_mn = HWS_mn
        self.HWS_mx = HWS_mx
        self.PRE_av = PRE_av
        self.PRE_mn = PRE_mn
        self.PRE_mx = PRE_mx
        self.WD_cp = WD_cp
