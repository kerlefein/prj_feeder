import requests
from requests.exceptions import RequestException
import json
import logging


logger = logging.getLogger(__name__)


class TelegramClient:
    def __init__(self, url, token):
        self.url = url
        self.token = token

    def send_message(self, chat_id, message_text, buttons=None) -> bool:
        url_api = '{}{}/sendMessage'.format(
            self.url,
            self.token,
        )
        headers = {'Content-type': 'application/json'}
        params = {
            'chat_id': chat_id,
            'text': message_text,
        }
        if buttons:
            params['reply_markup'] = {'inline_keyboard': buttons}
        try:
            response = requests.post(
                url_api,
                data=json.dumps(params),
                headers=headers
            )
        except RequestException as ex:
            logger.exception(ex)
            return False
        response = response.json()
        if response:
            return response['ok']
        return False

    def send_photo(self, chat_id, photo_url, message_text=None, buttons=None):
        url_api = '{}{}/sendPhoto'.format(
            self.url,
            self.token,
        )
        headers = {'Content-type': 'application/json'}
        params = {
            'chat_id': chat_id,
            'photo': photo_url,
        }
        if message_text:
            params['caption'] = message_text
        if buttons:
            params['reply_markup'] = {'inline_keyboard': buttons}
        try:
            response = requests.post(
                url_api,
                data=json.dumps(params),
                headers=headers
            )
        except RequestException as ex:
            logger.exception(ex)
            return False
        response = response.json()
        if response:
            return response['ok']
        return False

    def edit_message(self, chat_id, message_id, message_text, buttons=None):
        url_api = '{}{}/editMessageText'.format(
            self.url,
            self.token,
        )
        headers = {'Content-type': 'application/json'}
        params = {
            'chat_id': chat_id,
            'message_id': message_id,
            'text': message_text,
        }
        if buttons:
            params['reply_markup'] = {'inline_keyboard': buttons}
        try:
            requests.post(
                url_api,
                data=json.dumps(params),
                headers=headers
            )
        except RequestException as ex:
            logger.exception(ex)
