import logging
from domain import SubscriptonSource
from django.db import transaction
from updater.models import Users, Subscriptions, DeliveryCheck, SubscriptionInfo


logger = logging.getLogger(__name__)


class WebhookService:
    def __init__(self, telegram_client):
        self.telegram_client = telegram_client

    def handle_request(self, request):
        if 'callback_query' in request:
            chat_id = request['callback_query']['from']['id']
            message_id = request['callback_query']['message']['message_id']
            username = request['callback_query']['from']['username']
            callback = request['callback_query']['data']
            msg_log = ''.join((
                'CALLBACK: TRUE\n',
                'FROM: {}\n'.format(username),
                'CHATID: {}\n'.format(chat_id),
                'TEXT: {}\n'.format(callback),
                '===================================='
            ))
            logger.info(msg_log)
            self.handle_callback(chat_id, message_id, callback)
        else:
            chat_id = request['message']['chat']['id']
            username = request['message']['from']['username']
            text = request['message']['text']
            msg_log = ''.join((
                'FROM: {}\n'.format(username),
                'CHATID: {}\n'.format(chat_id),
                'TEXT: {}\n'.format(text),
                '===================================='
            ))
            logger.info(msg_log)
            if text == '/start':
                self.add_user(chat_id)
            elif text == '/help':
                self.send_help_message(chat_id)
            elif text == '/menu':
                self.send_menu(chat_id)
            elif text == '/about':
                self.send_about(chat_id)
            elif text == 'Marco':
                msg = 'Polo'
                self.universal_send(chat_id, msg)

    def handle_callback(self, chat_id, message_id, callback):
        if callback == 'goto_main_menu':
            self.send_menu(chat_id, message_id)
        else:
            if callback == 'my_subscriptions_menu':
                subs = Subscriptions.objects.get_user_subscriptions(chat_id)
                message_form = Menu.show_my_subscriptions(chat_id, message_id, subs)
            elif callback == 'all_subscriptions_menu':
                subs = [s.value for s in SubscriptonSource]
                message_form = Menu.show_all_subscriptions(chat_id, message_id, subs)
            elif callback == 'account_settings_menu':
                message_form = Menu.show_account_settings(chat_id, message_id)
            elif callback.startswith('from_all_subs_'):
                sub = callback[len('from_all_subs_'):len(callback)]
                brief_info = SubscriptionInfo.objects.get_brief_info(sub)
                if Subscriptions.objects.filter(user__chat_id=chat_id, source=sub).exists():
                    message_form = Menu.show_subscription_menu(chat_id, message_id, sub, brief_info, True)
                else:
                    message_form = Menu.show_subscription_menu(chat_id, message_id, sub, brief_info, False)
            elif callback.startswith('from_my_subs_'):
                sub = callback[len('from_my_subs_'):len(callback)]
                brief_info = SubscriptionInfo.objects.get_brief_info(sub)
                message_form = Menu.show_subscription_menu(chat_id, message_id, sub, brief_info, True)
            elif callback.startswith('subscribe_'):
                sub = callback[len('subscribe_'):len(callback)]
                with transaction.atomic():
                    user = Users.objects.get(chat_id=chat_id)
                    new_sub = Subscriptions.objects.create(
                        user_id=user.id,
                        source=sub,
                        account=None,  # this one is for future menu expansion
                    )
                    DeliveryCheck.objects.create(subscription_id=new_sub.id, delivered=False)
                message_form = Menu.add_subscription(chat_id, message_id, sub)
            elif callback.startswith('unsubscribe_'):
                sub = callback[len('unsubscribe_'):len(callback)]
                Subscriptions.objects.filter(
                    user__chat_id=chat_id,
                    source=sub,
                ).delete()
                message_form = Menu.remove_subscription(chat_id, message_id, sub)
            elif callback.startswith('info_'):
                sub = callback[len('info_'):len(callback)]
                full_info = SubscriptionInfo.objects.get_full_info(sub)
                message_form = Menu.show_suscription_info(chat_id, message_id, full_info)
            self.universal_edit(
                message_form.chat_id,
                message_form.message_id,
                message_form.msg,
                message_form.buttons,
            )

    def add_user(self, chat_id, username):
        if not Users.objects.filter(chat_id=chat_id).exists():
            Users.objects.create(chat_id=chat_id)
            msg = ''.join((
                'Welcome, {}!\n'.format(username),
                'Now try /menu command to see how can i serve you\n',
                '\n',
                'or /help to see list of commands',
            ))
        else:
            msg = ''.join((
                'Hey, {}, we already greet each other.\n'.format(username),
                'Please try /menu to manage services\n',
                'or /help if you find yourself stuck',
            ))
        self.telegram_client.send_message(chat_id, msg)

    def send_help_message(self, chat_id):
        msg = ''.join((
            'Here\'s a few commands that can help you navigate:\n',
            '/start - initiate registration.\n',
            '/help - you are here.\n',
            '/menu - main tool to manage services.\n',
            '/about - info about what can i do.\n',
            'Or you can send me Marco to get Polo in return.\n',
        ))
        self.telegram_client.send_message(chat_id, msg)

    def send_menu(self, chat_id, message_id=None):
        msg = 'MAIN MENU'
        buttons = [
            [
                Menu.generate_simple_button(
                    'My Subscriptions',
                    'my_subscriptions_menu',
                ),
                Menu.generate_simple_button(
                    'All subscriptions',
                    'all_subscriptions_menu',
                )
            ], [
                Menu.generate_simple_button(
                    'Account Settings',
                    'account_settings_menu',
                )
            ]
        ]
        if not message_id:
            self.telegram_client.send_message(chat_id, msg, buttons)
        else:
            self.telegram_client.edit_message(chat_id, message_id, msg, buttons)

    def send_about(self, chat_id):
        msg = ''.join((
            'I am a bot that can send updates of some resources.\n',
            'You can send me /help to see list of commands.',
            'Or /menu to manage services.',
        ))
        self.telegram_client.send_message(chat_id, msg)

    def universal_send(self, chat_id, msg, buttons=None):
        self.telegram_client.send_message(chat_id, msg, buttons)

    def universal_edit(self, chat_id, message_id, msg, buttons=None):
        self.telegram_client.edit_message(chat_id, message_id, msg, buttons)


class Menu:
    @staticmethod
    def show_all_subscriptions(chat_id, message_id, subs):
        num = 0
        msg = 'LIST OF ALL AVALIABLE SUBSCRIPTIONS: \n'
        for s in subs:
            num += 1
            msg += '\n' + str(num) + '. ' + s
        buttons = Menu.generate_sub_buttons(subs, 'from_all_subs_')
        buttons.append(
            [
                Menu.generate_simple_button(
                    'Main Menu',
                    'goto_main_menu',
                )
            ]
        )
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def show_my_subscriptions(chat_id, message_id, subs=None):
        if not subs:
            msg = 'NO ACTIVE SUBSCRIPTIONS'
            buttons = [
                [
                    Menu.generate_simple_button(
                        'All subscriptions',
                        'all_subscriptions_menu',
                    )
                ], [
                    Menu.generate_simple_button(
                        'Main Menu',
                        'goto_main_menu',
                    )
                ]
            ]
        else:
            num = 0
            msg = 'LIST OF ACTIVE SUBSCRIPTIONS: \n'
            for s in subs:
                num += 1
                msg += '\n' + str(num) + '. ' + s
            buttons = Menu.generate_sub_buttons(subs, 'from_my_subs_')
            buttons.append(
                [
                    Menu.generate_simple_button(
                        'Main Menu',
                        'goto_main_menu',
                    )
                ]
            )
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def show_account_settings(chat_id, message_id):
        msg = 'i guess this menu is useless rn'
        buttons = [[
            Menu.generate_simple_button(
                'Main Menu',
                'goto_main_menu',
            )
        ]]
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def show_subscription_menu(chat_id, message_id, sub, msg, active):
        action = 'Unsubscribe' if active else 'Subscribe'
        buttons = [
            [
                Menu.generate_simple_button(
                    action,
                    action.lower() + '_' + sub,
                ),
                Menu.generate_simple_button(
                    'Info',
                    'info_' + sub,
                )
            ], [
                Menu.generate_simple_button(
                    'Main Menu',
                    'goto_main_menu',
                )
            ]
        ]
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def show_suscription_info(chat_id, message_id, msg):
        buttons = [[
            Menu.generate_simple_button(
                'Main Menu',
                'goto_main_menu',
            )
        ]]
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def add_subscription(chat_id, message_id, sub):
        msg = ''.join((
            'SUCCESSSSSS!\n',
            sub.capitalize() + ' was activated!\n',
            'Enjoy\n',
        ))
        buttons = [[
            Menu.generate_simple_button(
                'Main Menu',
                'goto_main_menu',
            )
        ]]
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def remove_subscription(chat_id, message_id, sub):
        msg = ''.join((
            'Congratulations!\n',
            sub.capitalize() + ' was deactivated!\n',
            'From now on it won\'t disturbe you\n',
        ))
        buttons = [[
            Menu.generate_simple_button(
                'Main Menu',
                'goto_main_menu',
            )
        ]]
        return MessageForm(
            chat_id,
            message_id,
            msg,
            buttons,
        )

    @staticmethod
    def generate_simple_button(text, callback_data):
        return {
            'text': text,
            'callback_data': callback_data,
        }

    @staticmethod
    def generate_url_button(text, url):
        return {
            'text': text,
            'url': url,
        }

    @staticmethod
    def generate_sub_buttons(subs, callback_prefix):
        raw_buttons = []
        for s in subs:
            raw_buttons.append(
                Menu.generate_simple_button(
                    s,
                    callback_prefix + s,
                )
            )
        return Menu.combine_buttons(raw_buttons)

    @staticmethod
    def combine_buttons(raw_buttons):
        combined_buttons = []
        i = 0
        while i < len(raw_buttons):
            j = i
            row = []
            while i < len(raw_buttons) and i - j < 3:
                row.append(raw_buttons[i])
                i += 1
            if len(row) > 0:
                combined_buttons.append(row)
        return combined_buttons


class MessageForm:
    def __init__(self, chat_id, message_id, msg, buttons):
        self.chat_id = chat_id
        self.message_id = message_id
        self.msg = msg
        self.buttons = buttons
