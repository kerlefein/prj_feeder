import requests
from requests.exceptions import RequestException
import logging


logger = logging.getLogger(__name__)


class XKCDClient:
    def __init__(self):
        pass

    def retrieve_comics(self) -> object:
        url = 'http://xkcd.com/info.0.json'
        try:
            response = requests.get(url)
        except RequestException as ex:
            logger.exception(ex)
            return False
        comics = response.json()
        return XKCDDetails(
            comics['num'],
            comics['day'] + '.' + comics['month'] + '.' + comics['year'],
            comics['title'],
            comics['img'],
            comics['alt'],
        )


class XKCDDetails:
    def __init__(self, num, date, title, img, alt):
        self.num = num
        self.date = date
        self.title = title
        self.img = img
        self.alt = alt
