import enum


class SubscriptonSource(enum.Enum):
    XKCD = 'xkcd'
    APOD = 'apod'
    INSIGHT = 'insight'
