from django.db import models


class UserManager(models.Manager):
    def get_user_details(self) -> dict:
        user_details_list = self.all()
        return {user.id: user.chat_id for user in user_details_list}


class SubscriptionManager(models.Manager):
    def get_subscription_details(self, user_id_list) -> list:
        subscription_details = []
        for user_id in user_id_list:
            subscriptions = self.filter(user_id=user_id)
            for s in subscriptions:
                subscription_details.append(SubscriptionDetails(
                    s.id,
                    s.user_id,
                    s.account,
                    s.source,
                ))
        return subscription_details

    def get_user_subscriptions(self, chat_id) -> set:
        user_subscription_list = self.filter(user__chat_id=chat_id)
        return set([s.source for s in user_subscription_list])


class SubscriptionDetails:
    def __init__(self, id, user_id, account_name, source):
        self.id = id
        self.user_id = user_id
        self.account_name = account_name
        self.source = source


class DeliveryManager(models.Manager):
    def get_delivery_status(self) -> list:
        return list(self.filter(delivered=False).values_list('subscription_id', flat=True))

    def update_delivery_status(self, subscription_id):
        delivery = self.get(subscription_id=subscription_id)
        delivery.delivered = True
        delivery.save()


class UpdatePointManager(models.Manager):
    def get_update_points(self) -> dict:
        all_points = self.all()
        return {point.sub_name: point.last_point for point in all_points}

    def update_point(self, sub, new_point):
        point = self.get(sub_name=sub)
        point.last_point = new_point
        point.save()


class SubscriptionInfoManager(models.Manager):
    def get_brief_info(self, sub) -> str:
        sub_info = self.get(sub_name=sub)
        return sub_info.brief_info

    def get_full_info(self, sub) -> str:
        sub_info = self.get(sub_name=sub)
        return sub_info.full_info


class UpdatePointDetails:
    def __init__(self, name, point):
        self.name = name
        self.point = point


class Users(models.Model):
    chat_id = models.IntegerField()
    objects = UserManager()


class Subscriptions(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    source = models.CharField(max_length=50)
    account = models.CharField(max_length=50, null=True)
    objects = SubscriptionManager()


class DeliveryCheck(models.Model):
    subscription = models.ForeignKey(Subscriptions, on_delete=models.CASCADE)
    delivered = models.BooleanField()
    objects = DeliveryManager()


class UpdatePoint(models.Model):
    sub_name = models.CharField(max_length=50)
    last_point = models.CharField(max_length=999)
    objects = UpdatePointManager()


class SubscriptionInfo(models.Model):
    sub_name = models.CharField(max_length=50)
    brief_info = models.CharField(max_length=999)
    full_info = models.CharField(max_length=999)
    objects = SubscriptionInfoManager()
