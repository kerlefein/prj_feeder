from unittest import TestCase as UnitTestCase
from request_handler import Menu, WebhookService
from django.test import TestCase
from .models import Users, Subscriptions, DeliveryCheck
from collections import defaultdict
from domain import SubscriptonSource


class UnitTests(UnitTestCase):
    def test_generate_url_button(self):
        text = 'text'
        url = 'http://url.net'
        result = {
            'text': text,
            'url': url,
        }
        self.assertEqual(Menu.generate_url_button(text, url), result)


class UserRegistrationTest(TestCase):
    def test_add_user(self):
        request = {'update_id': 1, 'message': {'message_id': 355, 'from': {'id': 1, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'qwe', 'language_code': 'en'}, 'chat': {'id': 1, 'first_name': 'Игнатус', 'username': 'qwe', 'type': 'private'}, 'date': 1581005816, 'text': '/start', 'entities': [{'offset': 0, 'length': 6, 'type': 'bot_command'}]}}
        telegram_client = TestTelegramClient()
        service = WebhookService(telegram_client)
        service.handle_request(request)
        new_user_chat_id = Users.objects.filter(chat_id=request['message']['chat']['id']).exists()
        self.assertTrue(new_user_chat_id)


class SubscriptionTest(TestCase):
    def test_add_subscription(self):
        request = {'update_id': 647755682, 'callback_query': {'id': '236014426586669443', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 358, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581010257, 'edit_date': 1581010277, 'text': 'Choose action for apod:', 'reply_markup': {'inline_keyboard': [[{'text': 'Subscribe', 'callback_data': 'subscribe_apod'}, {'text': 'Info', 'callback_data': 'info_apod'}], [{'text': 'Main Menu', 'callback_data': 'goto_main_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'subscribe_apod'}}
        user = Users.objects.create(
            username=request['callback_query']['from']['username'],
            chat_id=request['callback_query']['message']['chat']['id'],
        )
        telegram_client = TestTelegramClient()
        service = WebhookService(telegram_client)
        service.handle_request(request)
        sub = request['callback_query']['data']
        sub = sub[len('subscribe_'):len(sub)]
        new_sub = Subscriptions.objects.get(user_id=user.id, source=sub)
        delivery = DeliveryCheck.objects.get(subscription_id=new_sub.id)
        self.assertEqual(new_sub.source, sub)
        self.assertFalse(delivery.delivered)

    def test_remove_subscription(self):
        request = {'update_id': 647755678, 'callback_query': {'id': '236014423897200228', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 358, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581010257, 'edit_date': 1581010260, 'text': 'Choose action for apod:', 'reply_markup': {'inline_keyboard': [[{'text': 'Unsubscribe', 'callback_data': 'unsubscribe_apod'}, {'text': 'Info', 'callback_data': 'info_apod'}], [{'text': 'Main Menu', 'callback_data': 'goto_main_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'unsubscribe_apod'}}
        user = Users.objects.create(
            username=request['callback_query']['from']['username'],
            chat_id=request['callback_query']['message']['chat']['id'],
        )
        sub = request['callback_query']['data']
        sub = sub[len('unsubscribe_'):len(sub)]
        old_sub = Subscriptions.objects.create(
            user_id=user.id,
            source=sub,
        )
        DeliveryCheck.objects.create(
            subscription_id=old_sub.id,
            delivered=False,
        )
        telegram_client = TestTelegramClient()
        service = WebhookService(telegram_client)
        service.handle_request(request)
        self.assertFalse(Subscriptions.objects.filter(user_id=user.id, source=sub).exists())
        self.assertFalse(DeliveryCheck.objects.filter(subscription_id=old_sub.id).exists())


class MenuTest(TestCase):
    def setUp(self):
        self.user = Users.objects.create(
            username='kerlefein',
            chat_id=54951390,
        )
        self.sub = Subscriptions.objects.create(
            user_id=self.user.id,
            source='apod',
        )
        self.delivery = DeliveryCheck.objects.create(
            subscription_id=self.sub.id,
            delivered=False,
        )
        self.telegram_client = TestTelegramClient()
        self.service = WebhookService(self.telegram_client)

    def test_show_all_subscriptions_quantity_of_buttons(self):
        request = {'update_id': 647755716, 'callback_query': {'id': '236014425065363721', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 374, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581092585, 'edit_date': 1581094145, 'text': 'MAIN MENU', 'reply_markup': {'inline_keyboard': [[{'text': 'My Subscriptions', 'callback_data': 'my_subscriptions_menu'}, {'text': 'All subscriptions', 'callback_data': 'all_subscriptions_menu'}], [{'text': 'Account Settings', 'callback_data': 'account_settings_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'all_subscriptions_menu'}}
        self.service.handle_request(request)
        quantity_of_subscriptions = len([s.value for s in SubscriptonSource])
        quantity_of_buttons = 0
        for row in self.telegram_client.params['edit_message']['buttons']:
            quantity_of_buttons += len(row)
        self.assertEqual(quantity_of_buttons, quantity_of_subscriptions + 1)

    def test_show_my_subscriptions_message_with_subscriptions(self):
        request = {'update_id': 647755718, 'callback_query': {'id': '236014423365087399', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 374, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581092585, 'edit_date': 1581105530, 'text': 'MAIN MENU', 'reply_markup': {'inline_keyboard': [[{'text': 'My Subscriptions', 'callback_data': 'my_subscriptions_menu'}, {'text': 'All subscriptions', 'callback_data': 'all_subscriptions_menu'}], [{'text': 'Account Settings', 'callback_data': 'account_settings_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'my_subscriptions_menu'}}
        self.service.handle_request(request)
        self.assertTrue(self.telegram_client.params['edit_message']['message_text'].startswith('LIST'))

    def test_show_my_subscriptions_message_without_subscriptions(self):
        request = {'update_id': 647755718, 'callback_query': {'id': '236014423365087399', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 374, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581092585, 'edit_date': 1581105530, 'text': 'MAIN MENU', 'reply_markup': {'inline_keyboard': [[{'text': 'My Subscriptions', 'callback_data': 'my_subscriptions_menu'}, {'text': 'All subscriptions', 'callback_data': 'all_subscriptions_menu'}], [{'text': 'Account Settings', 'callback_data': 'account_settings_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'my_subscriptions_menu'}}
        Subscriptions.objects.filter(user_id=self.user.id).delete()
        self.service.handle_request(request)
        self.assertTrue(self.telegram_client.params['edit_message']['message_text'].startswith('NO'))

    def test_show_my_subscriptions_quantity_of_buttons(self):
        request = {'update_id': 647755718, 'callback_query': {'id': '236014423365087399', 'from': {'id': 54951390, 'is_bot': False, 'first_name': 'Игнатус', 'username': 'kerlefein', 'language_code': 'en'}, 'message': {'message_id': 374, 'from': {'id': 988298033, 'is_bot': True, 'first_name': 'prj_feed_bot', 'username': 'MrFeederBot'}, 'chat': {'id': 54951390, 'first_name': 'Игнатус', 'username': 'kerlefein', 'type': 'private'}, 'date': 1581092585, 'edit_date': 1581105530, 'text': 'MAIN MENU', 'reply_markup': {'inline_keyboard': [[{'text': 'My Subscriptions', 'callback_data': 'my_subscriptions_menu'}, {'text': 'All subscriptions', 'callback_data': 'all_subscriptions_menu'}], [{'text': 'Account Settings', 'callback_data': 'account_settings_menu'}]]}}, 'chat_instance': '-7618799851170908059', 'data': 'my_subscriptions_menu'}}
        self.service.handle_request(request)
        quantity_of_subscriptions = len(Subscriptions.objects.filter(user_id=self.user.id))
        quantity_of_buttons = 0
        for row in self.telegram_client.params['edit_message']['buttons']:
            quantity_of_buttons += len(row)
        self.assertEqual(quantity_of_buttons, quantity_of_subscriptions + 1)


class TestTelegramClient:
    def __init__(self):
        self.params = defaultdict(list)

    def send_message(self, chat_id, message_text, buttons=None):
        self.params['send_message'] = {
            'chat_id': chat_id,
            'message_text': message_text,
            'buttons': buttons,
        }

    def send_photo(self, chat_id, photo_url, message_text=None, buttons=None):
        self.params['send_photo'] = {
            'chat_id': chat_id,
            'photo_url': photo_url,
            'message_text': message_text,
            'buttons': buttons,
        }

    def edit_message(self, chat_id, message_id, message_text, buttons=None):
        self.params['edit_message'] = {
            'chat_id': chat_id,
            'message_id': message_id,
            'message_text': message_text,
            'buttons': buttons,
        }
