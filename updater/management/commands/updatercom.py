from django.core.management.base import BaseCommand
from xkcd_client import XKCDClient
from tg_client import TelegramClient
from nasa_client import NASAClient
from domain import SubscriptonSource
from updater.models import Users, Subscriptions, DeliveryCheck, UpdatePoint
import logging
from django.conf import settings
from request_handler import Menu


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        user_details_dict = Users.objects.get_user_details()
        subsriptions = Subscriptions.objects.get_subscription_details(
            user_details_dict.keys()
        )
        xkcd_client = XKCDClient()
        nasa_client = NASAClient(
            settings.NASA_API_KEY,
        )
        telegram_client = TelegramClient(
            settings.TELEGRAM_API_URL,
            settings.TELEGRAM_API_TOKEN,
        )
        xkcd = xkcd_client.retrieve_comics()
        apod = nasa_client.retrieve_apod()
        insight = nasa_client.retrieve_insight()
        update_points = UpdatePoint.objects.get_update_points()
        delivery_failed_subscription_list = DeliveryCheck.objects.get_delivery_status()
        if delivery_failed_subscription_list:
            for failed_subscription in delivery_failed_subscription_list:
                logger.info('redelivery for subscription_id=%s', failed_subscription)
                DeliveryCheck.objects.update_delivery_status(failed_subscription)
        DeliveryCheck.objects.filter(delivered=True).update(delivered=False)
        for subscription in subsriptions:
            chat_id = user_details_dict[subscription.user_id]
            if subscription.source == SubscriptonSource.XKCD.value:
                if not xkcd:
                    continue
                if str(xkcd.num) == update_points[subscription.source]:
                    logger.info('No new comics yet (sub_id=%s)', subscription.id)
                    DeliveryCheck.objects.update_delivery_status(
                        subscription.id,
                    )
                else:
                    msg = ''.join((
                        xkcd.alt + '\n\n',
                        xkcd.title + ' - ' + xkcd.date,
                    ))
                    if telegram_client.send_photo(chat_id, xkcd.img, msg):
                        DeliveryCheck.objects.update_delivery_status(
                            subscription.id,
                        )
                    else:
                        logger.info('Failed to deliver (sub_id=%s)', subscription.id)
            elif subscription.source == SubscriptonSource.APOD.value:
                if not apod:
                    continue
                if apod.img == update_points[subscription.source]:
                    logger.info('No new pictures yet (sub_id=%s)', subscription.id)
                    DeliveryCheck.objects.update_delivery_status(
                        subscription.id,
                    )
                else:
                    msg = ''.join((
                        (apod.explanation + '\n\n') if apod.explanation else '',
                        (apod.title + '\n') if apod.title else '',
                        (apod.author + '\n') if apod.author else '',
                        (apod.date) if apod.date else '',
                    ))
                    if apod.img_hd:
                        button = [[
                            Menu.generate_url_button(
                                'Get HD photo',
                                apod.img_hd,
                            )
                        ]]
                    else:
                        button = None
                    if telegram_client.send_photo(chat_id, apod.img, msg, button):
                        DeliveryCheck.objects.update_delivery_status(
                            subscription.id,
                        )
                    else:
                        logger.info('Failed to deliver (sub_id=%s)', subscription.id)
            elif subscription.source == SubscriptonSource.INSIGHT.value:
                if not insight:
                    continue
                if insight.sol == update_points[subscription.source]:
                    logger.info('No new insight data yet (sub_id=%s)', subscription.id)
                    DeliveryCheck.objects.update_delivery_status(
                        subscription.id,
                    )
                else:
                    msg = ''.join((
                        'INSIGHT MISSION WEATHER DATA:\n\n',
                        'Mission sol No: ',
                        insight.sol + '\n',
                        'Current Mars season: ',
                        insight.season + '\n',
                        'Observation date range (UTC)\n',
                        insight.first_utc + ' - ' + insight.last_utc + '\n\n',
                        'ATMOSPHERIC TEMPERATURE (°F)\n',
                        'Average over sol: ',
                        str(insight.AT_av) + '\n',
                        'Minimum over sol: ',
                        str(insight.AT_mn) + '\n',
                        'Maximum over sol: ',
                        str(insight.AT_mx) + '\n\n',
                        'HORIZONTAL WIND SPEED (m/s)\n',
                        'Average over sol: ',
                        str(insight.HWS_av) + '\n',
                        'Minimum over sol: ',
                        str(insight.HWS_mn) + '\n',
                        'Maximum over sol: ',
                        str(insight.HWS_mx) + '\n',
                        'Wind direction (the wind is blowing from this direction ±11.25°): ',
                        insight.WD_cp + '\n\n',
                        'ATMOSPHERIC PRESSURE (Pa)\n',
                        'Average over sol: ',
                        str(insight.PRE_av) + '\n',
                        'Minimum over sol: ',
                        str(insight.PRE_mn) + '\n',
                        'Maximum over sol: ',
                        str(insight.PRE_mx) + '\n\n',
                    ))
                    if telegram_client.send_message(chat_id, msg):
                        DeliveryCheck.objects.update_delivery_status(
                            subscription.id,
                        )
                    else:
                        logger.info('Failed to deliver (sub_id=%s)', subscription.id)
        if xkcd:
            if str(xkcd.num) != update_points[SubscriptonSource.XKCD.value]:
                UpdatePoint.objects.update_point(SubscriptonSource.XKCD.value, str(xkcd.num))
        if apod:
            if apod.img != update_points[SubscriptonSource.APOD.value]:
                UpdatePoint.objects.update_point(SubscriptonSource.APOD.value, apod.img)
        if insight:
            if insight.sol != update_points[SubscriptonSource.INSIGHT.value]:
                UpdatePoint.objects.update_point(SubscriptonSource.INSIGHT.value, insight.sol)
        logger.info('======================================')
