# from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from request_handler import WebhookService
from django.conf import settings
from tg_client import TelegramClient
import logging


logger = logging.getLogger(__name__)

@csrf_exempt
def index(request):
    if request.method == "POST":
        logger.info(json.loads(request.body))
        telegram_client = TelegramClient(
            settings.TELEGRAM_API_URL,
            settings.TELEGRAM_API_TOKEN,
        )
        service = WebhookService(telegram_client)
        service.handle_request(json.loads(request.body))
    return HttpResponse()
